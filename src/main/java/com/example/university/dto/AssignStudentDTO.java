package com.example.university.dto;

import javax.validation.constraints.NotNull;

public class AssignStudentDTO {

    @NotNull
    private Integer studentId;

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }
}
