package com.example.university.rest;

import com.example.university.dto.AssignStudentDTO;
import com.example.university.exception.StudentAlreadyRegistered;
import com.example.university.service.CourseRegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/courses/{code}/registrations")
public class CourseRegistrationResource {

    CourseRegistrationService courseRegistrationService;

    @Autowired
    public CourseRegistrationResource(CourseRegistrationService courseRegistrationService) {
        this.courseRegistrationService = courseRegistrationService;
    }

    @PostMapping
    public ResponseEntity<Void> registerStudentToCourse(@PathVariable("code") final String code, @RequestBody AssignStudentDTO modelDto) throws StudentAlreadyRegistered {

        courseRegistrationService.registerStudentToCourse(code, modelDto.getStudentId());
        return new ResponseEntity<>(HttpStatus.OK);

    }



}
